function isIntArray(arr) {
    if(Array.isArray(arr)) {
        var result = true;
        for (var i = 0; i < arr.length; i++) {
            result = result && Number.isInteger(arr[i]);
        }
        return result;
    } else {
        return false;
    }
}

console.log(isIntArray([]));
console.log(isIntArray([1, 2, 3, 4]));
console.log(isIntArray([1, 2, 3, NaN]));